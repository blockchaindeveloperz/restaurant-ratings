'use strict'

const Validator = require('jsonschema').Validator;
const express = require('express')
const router = express.Router();
const restaurantController = require('../controllers/restaurant.controllers');
const jsonfile = require('jsonfile');
const restController = require('../controllers/restaurant-controller');


const validator = new Validator();

//Middleware?
router.put('/restaurant/:id', (req, res, next) => {
    if (!validator.validate(req.body, schema).valid) {
        res.status(400).write('Bad Request');
        next('router');
    } else
        next();
    },
    restaurantController.updateRestaurant
);

//http routes
router.post('/restaurant',restaurantController.postRestaurant);
router.get('/restaurant/:restId',restaurantController.getRestaurants);
router.get('/restaurants',restaurantController.getRestaurant);
//router.put('/restaurant/:restId',restaurantController.updateRestaurant);
router.delete('/restaurant/:restId',restaurantController.deleteRestaurant);


router.post('/restaurantInfo', restController.postrestaurantInfo);
router.put('/restaurantInfo',restController.putrestaurantInfo);
router.get('/restaurantInfo',restController.getallrestaurants);
router.get('/restaurantInfo/:restaurant_id',restController.getrestaurantInfo);
router.delete('/restaurantInfo/:restaurant_id',restController.deleterestaurant);

module.exports = router;
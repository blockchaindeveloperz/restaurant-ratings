//import { init, close, read, write, update } from '../helpers/db-utils';

const config = require('../config/config')
const http = require('http')
const MongoClient = require('mongodb').MongoClient
const dbutil = require('../helpers/db.utils')
const joi = require('joi')


module.exports.postRestaurant = async function (req, res) {
    /**
     * CREATE RESTAURANT CODE
     */

    //step 1. validate request body json with joi
    //step 1. validate request body json with joi
    const gradeSchema = joi.object().keys({
        date: joi.date().iso().required(),
        grade: joi.string().required(),
        score: joi.number().required()
    })

    const schema = joi.object().keys({


        address: joi.object({
            building: joi.string().required(),
            coord: joi.array().items(joi.number()).length(2),
            street: joi.string().required(),
            zipcode: joi.number().required()
        }).required(),
        borough: joi.string().required(),
        cuisine: joi.string().required(),
        grades: joi.array().items(gradeSchema),
        name: joi.string().required(),
        restaurant_id: joi.number().required()
    });
    joi.validate(req.body, schema, async (err, value) => {
        //step 2: if validation passes, post to db
        if (err) {
            console.log('rating did not match schema')

            return res.status(500).send('error, restaurant rating did not match schema')
        } else {
            try {
                await dbutil.post(value)
                return res.status(200).send(JSON.stringify(value))
            } catch (err) {
                console.log('error saving restaurant data to the database')
                return res.status(500).send('error saving restaurant data to the database')
            }
        }

    })

}

module.exports.getRestaurant = function (req, res) {
    /**
     * READ RESTAURANT WITH ID
     */
}

module.exports.getRestaurants = function (req, res) {
    /**
     * READ ALL RESTAURANTS
     */
}

module.exports.updateRestaurant = function (req, res) {
    /**
     * UPDATE A RESTAURANT WITH ID
     */

    let client = init(url);
    update(client, config.collection, req.body.restaurant_id, req.body)
        .then(write_result => {
            close(client);
            res.status(200).write(write_result);
        })
        .catch(err => {
            close(client);
            res.status('500').write(err);
        })
}

module.exports.deleteRestaurant = async (req, res) => {
    /**
     * DELETE A RESTAURANT WITH ID
     */

    // 1. Get the restaurant_id from the req params
    let restaurant_id = req.params.restId;


    //2. use restaurant_id to find the document & delete from mongoDB
    try {
        await dbutil.delete(restaurant_id);
        let response = {
            "restId": restaurant_id
        };
        return res.status(200).send(JSON.stringify(response));
    } catch (error) {
        logger.error("Could not delete restaurant", error);
        errorMsg.detail = "Could not delete restaurant";
        return res.status(500).send(errorMsg)
    }
}
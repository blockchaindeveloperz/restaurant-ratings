module.exports = {
    'url': 'mongodb://localhost:27017',
    'database': 'restaurants',
    'collection': 'restaurant-grades',
    'entity' : 'restaurant'
}
'use strict'

var config = require('../config/config.js');
var MongoClient = require('mongodb').MongoClient;

const url = config.url; //mongodb://localhost:27017/

module.exports = {
    post: function (restaurant) {
        if (restaurant == null || restaurant == '' || typeof restaurant == undefined || typeof restaurant != 'object') {
            throw new Error('invalid restaurant object')
        }
        console.log(restaurant)
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            var database = db.db(config.database);
            database.createCollection(config.collection, function (err, res) {
                if (err) throw err
                db.close()
            });

            database.collection(config.collection).insertOne(restaurant, function (err, res) {
                if (err) throw err
                console.log('restuarant rating posted')
                db.close()
            })
        })
    },
    delete: async (restId) => {
        MongoClient.connect(url, async (err, db) => {
            if (err) throw err;
            let dbo = await db.db(config.database);

            let query = { restId: restaurant_id };
            await dbo.collection(config.collection).deleteOne(query, async (err, obj) => {
                if (err) throw err;
                console.log(config.entity + " deleted");
                await db.close();
            });
        });
    }
}
//import { MongoClient as mongo} from 'mongodb';
//import config from '../config/config';

const mongo = require('mongodb').MongoClient;
const config = require('../config/config');

/**
 * 
 * @param url 
 */
module.exports.init = function (url) {
    return mongo.connect(url);
}

/**
 * 
 * @param {Promise<mongo>} client 
 */
module.exports.close = async function close (client) {
    return (await client).close();
}

/**
 * 
 * @param {Promise<mongo>} client 
 * @param {string} collection 
 * @param {*} key 
 */
module.exports.read = async function read (client, collection, key) {
    let database = (await client).db(config.database);

    try {
        let read = database.collection(collection).find(key);
    } catch {
        return Promise.reject('File Not Found!');
    }

    let results = [];
    while ( await read.hasNext() ) {
        let doc = await read.next();
        results.push(doc);
    }

    return results;
}

/**
 * 
 * @param {Promise<mongo>} client 
 * @param {string} collection 
 * @param {*} data 
 */
module.exports.write = async function write (client, collection, data) {
    let database = (await client).db(config.database);

    try {
        let write = database.collection(collection).insertOne(data);
        return Promise.resolve((await write).result);
    } catch {
        return Promise.reject('Not Writeable!');
    }
}

/**
 * 
 * @param {Promise<mongo>} client 
 * @param {string} collection 
 * @param {*} key 
 * @param {*} updates 
 */
module.exports.update = async function update (client, collection, key, updates) {
    let database = (await client).db(config.database);

    try {
        let update = database.collection(collection).findOneAndUpdate(key, updates);
        return Promise.resolve((await update).ok);
    } catch {
        return Promise.reject('Not Updatable!');
    }
}
'ust strict'

const express = require('express')
const bodyparser = require('body-parser')
const mongoose = require('mongoose')
const http = require('http')
const app = express()//initialize app
const host = process.env.HOST || 'localhost'
const port = process.env.PORT || '3000'


//set up app
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended: true}))
app.use(express.static('resources'));
console.log("*** Node application started ***")

//routes
const restaurantAPI = require('./app/routers/restaurant.routes')
app.use(restaurantAPI)

http.createServer(app).listen(3000, function(){
    console.log('*** server started ***')
    console.log('*** http://localhost:3000 ***')
})

module.exports = app

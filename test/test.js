const chai = require('chai')
const assert = require('chai').assert
const expect = require('chai').assert
const should = require('chai').should()
const app = require('../app')

chai.use(require('chai-http'))


describe('API endpoint /restaurants', function(){
    describe('POST tests', function(){
        it('should post a new restaurant rating',async() =>{
            return chai.request(app)
            .post('/restaurant')
            .send({
                "address": {
                    "building": "1007",
                    "coord": [ -73.856077, 40.848447 ],
                    "street": "Morris Park Ave",
                    "zipcode": "10462"
                 },
                 "borough": "Bronx",
                 "cuisine": "Bakery",
                 "grades": [
                    { "date": "2018-01-01T00:00:00Z", "grade": "B", "score": 14 }
                 ],
                 "name": "Morris Park Bake Shop",
                 "restaurant_id": "30075445"
            })
            .then(function(res){
                res.should.have.status(200);
                //res.should.be.json;
                res.body.should.be.an('object');
            })
        })

        it('should NOT post a new restaurant rating if has an invalid schema',async() =>{
            return chai.request(app)
            .post('/restaurant')
            .send({
                name: 'McDonalds',
                grade: 'F',
                address: '123 main st'
            })
            .then(function(res){
                res.should.have.status(500);
                //res.should.be.json;
                res.body.should.be.an('object');
            })
        })

    })

    describe('GET/Id tests', function(){

    })

    describe('GET tests', function(){

    })

    describe('PUT tests',function(){

    })

    describe('DELETE tests', function(){

    })
})